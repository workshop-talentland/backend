import { Request, Response } from 'express'

export const hello = (request: Request, response: Response) => {
  response.status(200).json({
    data: '¡hola mundo!',
  })
}
