import 'module-alias/register'
import express, { Express } from 'express'
import dotenv from 'dotenv'
import cors from 'cors'
import helmet from 'helmet'
import bodyParser from 'body-parser'

import { apiRoutes } from '@/routes'
import * as process from 'process'

dotenv.config()

const app: Express = express()
const port = process.env.PORT || 3000

app.use(cors())
app.use(helmet())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(apiRoutes)

app.listen(port, () => {
  console.log(`[CDV]: Server is running at http://localhost:${port}`)
})
