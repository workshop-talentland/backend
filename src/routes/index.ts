import express from 'express'
import { hello } from '@/endpoints'

export const apiRoutes = express.Router()

apiRoutes.use('/', hello)
